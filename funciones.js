var imprimirNombreCliente=false;
var SemanaEmpiezaEn=1; //1 lunes, 0 domingo
var comercioNombre=localStorage.comercioNombre||"Despensa";

function capitalize(str) {
	str=str||'';
	var lis=str.split(" ");
	lis=lis.map(v=>{
		if (v.length>=2) {
			v=v.substr(0,1).toUpperCase()+v.substr(1).toLowerCase();
		}
		return v;
	});
	return lis.join(" ");
}

function enMoneda(num) {
	var str=(Math.round(num*100)/100)+'';
	if (str.indexOf('.')==-1) {
		str+='.';
	}
	str+='00';
	str=str.substr(0,str.lastIndexOf('.')+3);
	return '$ '+str;
}

function minimo(n,m) {
	if (n<m) return m;
	else return n;
}

function maximo(n,m) {
	if (n>m) return m;
	else return n;
}

function haceTiempo(time) {
	var ti=(Date.now()/1000-time/1000);
	if (ti<=60) {
		return "reciente";
	} else if (ti<60*60) {
		return "hace "+Math.round(ti/60)+" mins";
	} else if (ti<60*60*24) {
		return "hace "+Math.round(ti/60/60)+" hs";
	} else if (ti<60*60*24*31) {
		return "hace "+Math.round(ti/60/60/24)+" dias";
	} else if (ti<60*60*24*366) {
		return "hace "+(Math.round(ti/60/60/24/31*10)/10)+" meses";
	} else if (ti<60*60*24*366*10) {
		alert(ti+' < '+60*60*24*366*10);
		return "hace "+(Math.round(ti/60/60/24/366*10)/10)+" años";
	} else {
		return "nunca";
	}

}

function enTiempo(time) {
	var ti=time/1000;
	if (ti<=60) {
		return Math.round(ti)+" secs";
	} else if (ti<60*60) {
		return Math.round(ti/60)+" mins";
	} else if (ti<60*60*24) {
		return Math.round(ti/60/60)+" hs";
	} else if (ti<60*60*24*31) {
		return Math.round(ti/60/60/24)+" dias";
	} else if (ti<60*60*24*366) {
		return (Math.round(ti/60/60/24/31*10)/10)+" meses";
	} else if (ti<60*60*24*366*10) {
		return (Math.round(ti/60/60/24/366*10)/10)+" años";
	} else {
		//return "nunca";
	}
}

function getPrecioDif(prod,cant) {
	if (!prod)
		return 0;
	var reto=prod.precio_venta;
	if (prod.precio_dif) {
		var nprecio=prod.precio_dif.filter(vv=>vv.cant<=cant).sort((a,b)=>b.cant-a.cant);
		if (nprecio.length) {
			console.log(nprecio);
			reto=nprecio[0].precio;
		}
	}
	return reto;
}

function actualizarPrecioDif(prod) {
	if (prod.precio_dif&&prod.precio_dif.length) {
		prod.precio_dif.forEach(v=>{
			v.precio=getFloat("Precio de "+prod.nombre+" x"+v.cant+" (precio unitario)",prod.precio_venta);
		});
	}
}

function enCantidad(num,solotxt) {
	var str=(Math.round(num*1000)/1000)+'';
	if (str.indexOf('.')==-1) {
		str+='.';
	}
	str+='000';
	str=str.substr(0,str.lastIndexOf('.')+4);
	if (!solotxt) str=str.split('.').join("<font class='subCant'>.")+"</font>";
	return "x "+str;
}

function getFloat(txt,def,pretxt,cancelableDefault) {
	var inn=prompt((pretxt||'')+txt,def)||"";
	var num=parseFloat(inn.split(',').join('.'));
	if (num==null||isNaN(num)) {
		if (!cancelableDefault&&cancelableDefault!==0) {
			return getFloat(txt,def,"OTRA VEZ. ");
		} else {
			return cancelableDefault;
		}
	}
	return num;
}

function getText(txt,def,opc,pretxt) {
	if (!opc)
		opc={};
	var inn=prompt((pretxt||'')+txt,def);
	if (!inn||(opc.min&&inn.length<opc.min)||(opc.max&&inn.length>opc.max)) {
		return getText(txt,def,opc,"OTRA VEZ. ");
	}
	return inn;
}

function subrayar(txt,sub) {
	txt=txt+'';
	if (!sub||!sub.length||!txt.length)
		return txt;
	var rx=new RegExp(sub,"gi");
	var i=0;
	var ret="";
	while (i<txt.length) {
		var s=txt.search(rx);
		if (s==-1) {
			i=txt.length;
		}
		else {
			ret+=txt.substr(0,s)+"<u>"+txt.substr(s,sub.length)+"</u>";
			txt=txt.substr(s+sub.length);
		}
	}
	ret+=txt;
	return ret;
}

function str_pad(str,len,dir,chr) {
	str=str||'';
	chr=chr||'0';
	while (str.length<len) {
		if (!dir||dir=='LEFT') {
			str=chr+str;
		} else {
			str=str+chr;
		}
	}
	return str;
}

function getCliente(opciones,pretxt) {
	if (!db_clientes)
		return;
	if (!opciones||opciones.length==0) {
		opciones=db_clientes;
		pretxt=opciones?pretxt:"";
	}
	if (opciones.length>6) {
		var filtro=prompt((pretxt||'')+"Buscar cliente: ",'N/A');
		if (filtro=='N/A')
			return db_clientes[0];
		else if (filtro) {
			opciones=opciones.filter(v=>v.nombre.toLowerCase().indexOf(filtro.toLowerCase())!=-1);
			if (opciones.length==0) {
				if (confirm("Encontrados 0 clientes.\n---------------------\nCliente NUEVO?")) {
					var nombre=capitalize(getText("(CLIENTE NUEVO) Apellido y nombre: ",'',{min:3}));
					db_clientes.push({cid:db_clientes.length+1,nombre:nombre,estado:0,ultima_compra:0,ultimo_pago:0});
					localStorage.setItem("db_clientes",JSON.stringify(db_clientes));
					return db_clientes[db_clientes.length-1];
				} else
					return getCliente();
			} else
				return getCliente(opciones,"Encontrados: "+opciones.length+" clientes.\n");
		} else {
			//nada?
		}
	} else {
		var sele=prompt("Elije nro. de cliente: \n---------------------\n0 -> NUEVO CLIENTE.\n"+opciones.map((v,i)=>(i+1)+" -> "+v.nombre+".").join("\n"),'Buscar de nuevo');
		if (sele=='0') {
			var nombre=capitalize(getText("(CLIENTE NUEVO) Apellido y nombre: ",'',{min:3}));
			var ncid=db_clientes[db_clientes.length-1].cid;
				ncid=(ncid=='N/A'?1:parseInt(ncid)+1);
			db_clientes.push({cid:ncid,nombre:nombre,estado:0,ultima_compra:0,ultimo_pago:0});
			localStorage.setItem("db_clientes",JSON.stringify(db_clientes));
			return db_clientes[db_clientes.length-1];
		} else if (parseInt(sele)>=1&&parseInt(sele)<=9) {
			return opciones[parseInt(sele)-1];
		} else {
			return getCliente();
		}
	}
}

function guardarVenta(cliente,solopago) {
	if (!db_ventas)
		return;
	var nventa={vid:0,items:[],total:0,fecha:Date.now(),cliente:cliente.cid,pago:0,vuelto:0,saldada:false};
	if (!solopago) {
		var arr=dom_divlistashop.querySelectorAll(".itemcontainer");
		for (var i=0;i<arr.length;i++) {
			nventa.items.push({cod:arr[i].codigo,cantidad:arr[i].cantidad,precio:arr[i].precio,subtotal:arr[i].subtotal});
			nventa.total+=arr[i].subtotal;
		}
	}
	var pago=getFloat("("+cliente.nombre+") Paga efectivo: ",'0');
	nventa.pago=pago;
	nventa.vuelto=pago-nventa.total;
	if (pago!=0||nventa.total!=0) {
		var estadoPrevio=cliente.estado;
		cliente.estado+=nventa.vuelto;
		if (cliente.estado>0)
			cliente.estado=0;
		if (pago>0)
			cliente.ultimo_pago=Date.now();
		if (nventa.total>0)
			cliente.ultima_compra=Date.now();
		if (nventa.pago>=nventa.total)
			nventa.saldada=true;
		var vid=db_ventas.length?(db_ventas[db_ventas.length-1].vid+1):1;
		nventa.vid=vid;
		db_ventas.push(nventa);
		localStorage.setItem('db_ventas',JSON.stringify(db_ventas));
		localStorage.setItem('db_clientes',JSON.stringify(db_clientes));
		/*Stockear*/
		nventa.items.forEach(v=>{
			var pid=db_productos.find(w=>w.cod==v.cod);
			if (pid&&pid.stock!=-1) {
				pid.stock-=v.cantidad;
				if (pid.stock<0)
					pid.stock=0;
			}
		});
		localStorage.setItem('db_productos',JSON.stringify(db_productos));
		/*Fin Stock*/
		if (solopago||nventa.total==0) {
			alert((cliente.estado<0?cliente.nombre+" sigue debiendo: "+enMoneda(-cliente.estado):"Vuelto: "+enMoneda(nventa.pago+estadoPrevio))+"\n-----------------------------------\nPago registrado. #"+vid
				+(cliente.cid!='N/A'?"\nEstado de deuda: "+enMoneda(-cliente.estado):""));
			make_log("Pago #"+vid+": "+enMoneda(nventa.pago)+". Vuelto: "+enMoneda(minimo(nventa.pago+estadoPrevio,0))+". Cliente: "+cliente.nombre+". Estado de deuda: "+enMoneda(cliente.estado));
			if (cliente.cid!='N/A'&&cliente.estado>=0) {
				db_ventas.forEach(v=>{
					if (!v.saldada)
						v.saldada=true;
				});
			}
			localStorage.setItem('db_ventas',JSON.stringify(db_ventas));
		} else {
			alert((nventa.vuelto<0?cliente.nombre+" queda debiendo: "+enMoneda(-nventa.vuelto):"Vuelto: "+enMoneda(minimo(nventa.vuelto,0)))+"\n-----------------------------------\nVenta registrada. #"+vid
				+(cliente.cid!='N/A'?"\nEstado de deuda: "+enMoneda(-cliente.estado):""));
			make_log("Venta #"+vid+": "+enMoneda(nventa.total)+". Pago: "+enMoneda(nventa.pago)+". Vuelto: "+enMoneda(minimo(nventa.vuelto,0))+". Cliente: "+cliente.nombre+". Estado de deuda: "+enMoneda(cliente.estado));
			if (conTicket==true&&confirm("¿Imprimir Ticket?")) {
				imprimirVenta(nventa);
			}
		}
	} else {
		alert("(Modo consulta)"
			+"\n"+cliente.nombre
			+"\n---------------------------"
			+"\nEstado de cuenta: "+enMoneda(cliente.estado)
			+"\nÚlt.compra: "+haceTiempo(cliente.ultima_compra)
			+"\nÚlt.pago: "+haceTiempo(cliente.ultimo_pago));
	}
}

function imprimirVenta(ven) {
	var cli=db_clientes.find(v=>v.cid==ven.cliente)||db_clientes[0];
	var cum=str_pad("",32,null,"#")+"\n"
		+"* "+comercioNombre+" "+enFecha(ven.fecha)+" "+enHora(ven.fecha)+"\n"
		+(imprimirNombreCliente?"* "+cli.nombre:"")+"\n";
	ven.items.forEach(v=>{
		var pro=db_productos.find(p=>p.cod==v.cod)||db_productos[0];
		cum+=pro.nombre+"\n"
			+str_pad(enCantidad(v.cantidad,true),12,'RIGHT',' ')
			+str_pad(enMoneda(v.subtotal),20,'LEFT',' ')
			+"\n";
	});
	cum+=str_pad("TOTAL: "+enMoneda(ven.total),32,'LEFT',' ')+"\n"
		+str_pad("Pago: "+enMoneda(ven.pago),32,'LEFT',' ')+"\n"
		+str_pad("Vuelto: "+enMoneda(minimo(ven.vuelto,0)),32,'LEFT',' ')+"\n";
	cum+="-Ticket no valido como factura.-\n";
	cum+=str_pad("",32,null,"#")+"\n";
	// alert(cum);
	make_ticket(cum);
}

function recalcularVentas(cod,npre) {
	if (!db_ventas||!db_productos||!db_clientes)
		return;
	var prod=db_productos.find(v=>v.cod==cod);
	var modifV=false;
	var modifC=false;
	db_ventas.forEach(v=>{
		if (!v.saldada) {
			var dy=v.total;
			v.total=0;
			v.items.forEach(i=>{
				if (i.cod==cod) {
					var ndpre=getPrecioDif(prod,i.cantidad)||npre;
					if (i.precio<ndpre) {
						i.precio=ndpre;
						i.subtotal=i.precio*i.cantidad;
						modifV=true;
					}
				}
				v.total+=i.subtotal;
			});
			dy-=v.total;
			if (dy<0&&v.cliente!='N/A') {
				var pind=db_clientes.findIndex(c=>c.cid==v.cliente);
				if (pind!=-1) {
					db_clientes[pind].estado+=dy;
					modifC=true;
				}
			}
		}
	});
	if (modifV) {
		localStorage.setItem("db_ventas",JSON.stringify(db_ventas));
	}
	if (modifC) {
		localStorage.setItem("db_clientes",JSON.stringify(db_clientes));
	}
}

function recalcularFiados(cliente) {
	if (!db_ventas||!db_productos||!db_clientes) {
		alert("error c1");
		return;
	}

	if (cliente.cid=='N/A') {
		alert("error c3");
		return;
	}
	if (typeof cliente=='number') {
		cliente=db_clientes.find(c=>c.cid==cliente);
	}
	if (!cliente) {
		alert("error c2");
		return;
	}

	var modifC=false;
	cliente.estado=0;

	db_ventas.forEach(v=>{
		if (!v.saldada&&v.cliente!='N/A'&&v.cliente==cliente.cid) {
			cliente.estado+=v.pago;
			cliente.estado-=v.total;
			modifC=true;
		}
	});
	if (modifC) {
		localStorage.setItem("db_clientes",JSON.stringify(db_clientes));
	}
}

function enLiquidez(cod,modotxt) {
	if (!db_ventas)
		return;
	cod=cod+'';
	var tot=0;
	var ago=0;
	var i=db_ventas.length-1;
	while (ago<60*60*24*31&&i>0) {
		db_ventas[i].items.forEach(v=>{
			if (v.cod==cod)
				tot+=v.cantidad;
		});
		i--;
		ago=Date.now()-(db_ventas[i].fecha||(Date.now()-60*60*24));
		if (tot>100&&ago>60*60*24*7) {
			i=-1;
		}
	}
	if (modotxt) {
		if (tot==0)
			return "sin ventas en el último mes"
		else
			return enCantidad(tot)+" / "+enTiempo(ago);
	}
	else
		return ({cant:tot,tiempo:ago});
}

function enVisitas(cid,modotxt) {
	if (!db_ventas)
		return;
	cid=cid+'';
	var cant=0;
	var totv=0;
	var totp=0;
	var ago=0;
	var i=db_ventas.length-1;
	while (ago<60*60*24*31&&i>0) {
		if (db_ventas[i].cliente==cid) {
			cant++;
			totv+=db_ventas[i].total;
			totv+=db_ventas[i].pago;
		}
		i--;
		ago=Date.now()-(db_ventas[i].fecha||(Date.now()-60*60*24));
	}
	if (modotxt) {
		if (cant==0)
			return "sin visitas en el último mes"
		else
			return enMoneda(totv)+" (en "+cant+" visitas) / "+enTiempo(ago);
	}
	else
		return ({cant:cant,total:totv,pagos:totp,tiempo:ago});
}

function enHora(time) {
	var d=new Date(time);
	var h=("0"+d.getHours()).substr(-2);
	var m=("0"+d.getMinutes()).substr(-2);
	return h+":"+m;
}

function enFecha(time) {
	var d=new Date(time);
	var h=("0"+d.getDate()).substr(-2);
	var m=("0"+(d.getMonth()+1)).substr(-2);
	var y=("0"+(d.getYear())).substr(-2);
	return h+"/"+m+"/"+y;
}

function enSemana(time) {
	var d=new Date(time);
		d=new Date(time-(fixDay(d.getDay()-SemanaEmpiezaEn))*1000*60*60*24);
	var f=new Date(d.getTime()+1000*60*60*24*6);
	var h=("0"+d.getDate()).substr(-2);
	var hh=("0"+f.getDate()).substr(-2);
	var m=("0"+(d.getMonth()+1)).substr(-2);
	var y=("0"+(d.getYear())).substr(-2);
	return h+"-"+hh+"/"+m+"/"+y;
}

function fixDay(d) {
	if (d<0) {
		d=d+7;
		return fixDay(d);
	}
	return d
}

var db_dias=['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
function enDia(time) {
	var d=new Date(time);
	return db_dias[d.getDay()];
}
if (window&&window.parent) {
	function make_log(str) {
		window.parent.make_log(str);
	}
	function make_ticket(str) {
		window.parent.make_ticket(str);
	}
}
