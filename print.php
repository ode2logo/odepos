<?php
function leerfile($url)
{
  if (file_exists($url))
  {
    $handle=fopen($url,'r');
  }
  else
  {
    $handle=fopen($url,'w+');
  }
  $len=filesize($url);
  $cont='';
  if ($len>0)
  {
    $cont=fread($handle,$len);
  }
  fclose($handle);
  return $cont;
}

require __DIR__ . '/escpos-php/autoload.php';
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;
$connector = new FilePrintConnector("php://stdout");
// $connector = new FilePrintConnector("print.txt");
$printer = new Printer($connector);
$printer -> text(leerfile("preprint.txt")."\n");
$printer -> cut();
$printer -> close();
shell_exec("copy print.txt com1");
